const mongoose = require('mongoose');
// const bankDB = require('../middlewares/dbConnection');

const accSchema = new mongoose.Schema({
    bUser:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    bName:{
        type:String,
        required: true
    },
    bBranch:{
        type: String,
        required: true
    },
    bIFSC:{
        type: String,
        reuired: true
    },
    bCity:{
        type: String,
        required: true
    },
    bAccType:{
        type: String,
        default: "Saving"
    },
    bAccNumber:{
        type: Number
    },
    bAmount:{
        type: Number,
        default:0
    }
},{timestamps:true});

const Account = mongoose.model('account',accSchema);

module.exports = Account;
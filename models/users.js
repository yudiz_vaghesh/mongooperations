const mongoose = require('mongoose');
// const connDB = require('../middlewares/connectionPool');


const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    mobile:{
        type: Number,
        required: true
    },
    dob:{
        type: Date,
        required: true
    },
    isAccCreated:{
        type: Boolean,
        default: false
    },
    authToken:String
},{timestamps:true});

const User = mongoose.model('user',userSchema);

module.exports = User;
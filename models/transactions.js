const mongoose = require('mongoose');
const bankDB = require('../middlewares/dbConnection');

const transSchema = new mongoose.Schema({
    fromUser:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'User'
    },
    fromAccount:{
        type:mongoose.Schema.Types.ObjectId.apply,
        ref:'Account'
    },
    toAccount:{
        type: mongoose.Schema.Types.ObjectId,
        ref:'Account'
    },
    amount:{
        type: Number,
        required: true
    },

},{timestamps:true});

const Transaction = bankDB.model('transaction',transSchema);

module.exports = Transaction;
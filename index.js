const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/users');
const accRoutes = require('./routes/accounts');
const adminRoutes = require('./routes/admin');
const verifyPool = require('./routes/verifyPool');

// initialize app
var app = express();

// use the bodyParser for form or post data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extendedL:false}));

// use application routes
app.use('/users',userRoutes);
app.use('/accounts',accRoutes);
app.use('/admin',adminRoutes);
app.use('/pool',verifyPool);
// dbConnection
// require('./middlewares/dbConnection'); 
// connection pool file
// require('./middlewares/connectionPool');

require('./middlewares/connectionPool');
//run localhost server
app.listen(5000,()=>{
    console.log('Localhost connected on 5000...');
});



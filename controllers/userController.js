
const User = require('../models/users');
const jwt = require('jsonwebtoken');
const Account = require('../models/accounts');
const mongoose = require('mongoose');


// Create User
const postUser = async (req,res)=>{
    try{
        let user = new User({
            name: req.body.name,
            email: req.body.email,
            mobile: req.body.mobile,
            dob: req.body.dob
        });
        let token = this.generateToken(user.name,user.email);
        user.authToken = await token;
        user = await user.save();
        res.send(user);
    }catch(error)
    {
        console.log(error);
    }
}

// Add Amount
const addAmount = async(req,res)=>{
    
    try{
            // check user is authrized or not
        let aUser = await User.findOne({name:req.user.id});
        if(!aUser) return res.status(404).send('User Not Found');

        //check amount in account collection for this user
        let uAccount = await Account.updateOne(
            {bUser:aUser._id,bName:req.body.bank},
            {$inc:{bAmount:req.body.amt}}
        );

        res.send(uAccount);
    }catch(error){
        console.log(error);
    }
    
}

const sendMoney = async (req,res)=>{
    
        // currently this transaction works for static users
            const session = await mongoose.startSession();
            session.startTransaction();
            try {
              const opts = { session, new: true };
              const sender = await Account.
                findOneAndUpdate({ bUser:'60e7f1788377df1114394cb0',bName:"ICICI Bank"}, { $inc: { bAmount: -5 } }, opts);
              if (sender.bAmount < 0) {
                // If A would have negative balance, fail and abort the transaction
                // `session.abortTransaction()` will undo the above `findOneAndUpdate()`
                throw new Error('Insufficient funds: ' + (sender.bAmount + 1));
              }
          
              const reciever = await Account.
                findOneAndUpdate({ bUser: '60e8295cd2c11e2008b37492',bName:"ICICI Bank"}, { $inc: { bAmount: 5 } }, opts);
          
              await session.commitTransaction();
              session.endSession();
              res.send({ from: sender, to: reciever });
            } catch (error) {
              // If an error occurred, abort the whole transaction and
              // undo any changes that might have happened
              await session.abortTransaction();
              session.endSession();
              throw error; // Rethrow so calling function sees error
            }
}

const getAllusers = async (req,res)=>{
        let data = await User.find();  
        res.send(data);
}

// send money async function
// The actual transfer logic

// generate token
module.exports.generateToken = (id,email)=>{
    return jwt.sign({id,email},'user auth',{expiresIn: 30 * 60 * 1000});
}

// admin token
module.exports.generateAdminToken = (id,email)=>{
    return jwt.sign({id,email},'admin master');
}
// export all controllers
module.exports = {postUser,addAmount,sendMoney,getAllusers};
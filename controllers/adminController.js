const Account = require('../models/accounts');


const addFields = async (req,res)=>{
    try{

        let newFields = await Account.aggregate([
            {$group:{_id:"$bName",totalUsers:{$push:{user:"$bUser"}}}},
            {$addFields:{"isVerified":true}},
            {$project:{_id:1,totalUsers:1,isVerified:1}}
        ]);

        res.send(newFields);
    }  catch(err)
    {
        console.log(err);
    }
};

const append = async(req,res)=>{
    try{

        let bankGroups = [
            {$group:{_id:"$bName",totalUsers:{$push:{user:"$bUser"}}}},
            {$project:{_id:1,totalUsers:1}}
        ];

        let banks = await Account.aggregate().append(bankGroups);

        res.send(banks);
    }  catch(err)
    {
        console.log(err);
    }
};

const count = async(req,res)=>{
    try{

        let count = await Account.aggregate([
            {$match:{bAmount:{$gt:10000}}},
            {$count:"Rich Accounts"}
        ]);
        res.send(count);
    }catch(err)
    {console.log(err);}
}

const facet = async(req,res)=>{
    try{

        let multi = await Account.aggregate([
            {$facet:{
                bankDetails:[
                    {$group:{_id:"$bName",totalUsers:{$push:{User:"$bUser"}}}},
                    {$project:{_id:1,totalUsers:1}}
                ],
                userDetails:[
                    {$group:{_id:"$bUser",Accounts:{$push:{Bank:"$bName"}}}}
                ]
            }}
        ]);

        res.send(multi);

    }catch(err){console.log(err);}
}

const lookup = async(req,res)=>{
    try{
        let doc = await Account.aggregate([
            {
                $lookup:{
                    from: 'users',
                    localField:"bUser",
                    foreignField:"_id",
                    as:"Users"
                }
             },
             {$unwind:"$Users"},
             {$project:{bName:1,Users:1}}
        ]);

        res.send(doc);
    }catch(err){console.log(err);}
}

const replaceRoot = async(req,res)=>{
    try{
        
        let doc = await Account.aggregate([
            {
                $lookup:{
                    from: 'users',
                    localField:"bUser",
                    foreignField:"_id",
                    as:"Users"
                }
             },
             {$replcaeRoot:{newRoot:"$Users"}}
        ]);

        res.send(doc);
    }catch(err){console.log(err);}
}

module.exports = {
    addFields,
    append,
    count,
    facet,
    lookup,
    replaceRoot
};
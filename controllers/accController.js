const Account = require('../models/accounts');
const User = require('../models/users')

const getAcc = (req,res)=>{
    res.send(req.user);
}
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min) + min); //The maximum is exclusive and the minimum is inclusive
}
const postAcc = async (req,res)=>{

    try{

        // Find user based on auth token for creating account
        let user = await User.findOne({name:req.user.id});
        if(!user) return res.status(404).send('User not found !!!');
    
        // check that whether this user's account in this bank is created or not
        let vAccount = await Account.findOne({bUser:user._id,bName:req.body.name});
        if(vAccount) return res.status(400).send('User Account is Already created.');

        // provide details to creating account
        let account = new Account({
            bUser: user._id,
            bName: req.body.name,
            bBranch:req.body.branch,
            bIFSC: req.body.ifsc,
            bCity: req.body.city,
            bAccType: req.body.accType,
            bAccNumber: getRandomInt(111111111111,999999999999),
            bAmount: req.body.amount
        });

       
        user.isAccCreated = true;
        user = await user.save()
        //if user is new then create account 
        account = await account.save();
        res.send(account);
    }catch(error)
    {
        console.log(error);
    }
    // res.send(user);

}
module.exports = { getAcc, postAcc }
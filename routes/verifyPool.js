const express = require('express');
const Account = require('../models/accounts');

var router = express.Router();



router.get('/',async (req,res)=>{
    // await test();
    console.log('Welcome into testing of connection poolsize 6');
    var sTime = Date.now()
    
    // request 1 
    let ICICIUsers = await Account.aggregate([
        {$match:{"bName":"ICICI Bank"}},
        {$group:{_id:"$bName", totalReserve:{$sum:"$bAmount"}}},
        {$project:{_id:0}}
    ])
    console.log('Execution time for icici ',Date.now()-sTime,'ms');
    // request 2
    let SBIUsers = await Account.aggregate([
        {$match:{"bName":"SBI Bank"}},
        {$group:{_id:"$bName", totalReserve:{$sum:"$bAmount"}}},
        {$project:{_id:0}}
    ])
    console.log('Execution time for SBI',Date.now()-sTime,'ms');
    // request 3
    let DenaUsers = await Account.aggregate([
        {$match:{"bName":"Dena Bank"}},
        {$group:{_id:"$bName", totalReserve:{$sum:"$bAmount"}}},
        {$project:{_id:0}}
    ])
    console.log('Execution time for dena',Date.now()-sTime,'ms');
    // request 4
    let AxisUsers = await Account.aggregate([
        {$match:{"bName":"Axis Bank"}},
        {$group:{_id:"$bName", totalReserve:{$sum:"$bAmount"}}},
        {$project:{_id:0}}
    ])
    console.log('Execution time for axis',Date.now()-sTime,'ms');
    // request 5
    let PNBUsers = await Account.aggregate([
        {$match:{"bName":"Punjab National Bank"}},
        {$group:{_id:"$bName", totalReserve:{$sum:"$bAmount"}}},
        {$project:{_id:0}}
    ])
    console.log('Execution time for PNB',Date.now()-sTime,'ms');
   res.send({SBI:SBIUsers,ICICI:ICICIUsers,DENA:DenaUsers,AXIS:AxisUsers,PNB:PNBUsers});
});


module.exports = router;
const express = require('express');
const accController = require('../controllers/accController');
const auth = require('../middlewares/auth');
var router = express.Router();


router.get('/',(req,res)=>{
    res.send('Hello world');
})


router.post('/create',auth,accController.postAcc);

module.exports = router;
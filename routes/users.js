const express = require('express');
const userController = require('../controllers/userController');
const auth = require('../middlewares/auth');
var router = express.Router();


router.get('/',(req,res)=>{
    global.mDB.listCollections({}).toArray((err,collections)=>{
        collections.forEach(element => {
            console.log(element);
        });
    })
    res.send('Hello world');
})


router.post('/add',userController.postUser);
router.put('/addAmount',auth,userController.addAmount)
router.post('/send',userController.sendMoney);
router.get('/all',userController.getAllusers);
module.exports = router;
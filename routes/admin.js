const express = require('express');
const adminController = require('../controllers/adminController');
const adminAuth = require('../middlewares/adminAuth');
var router = express.Router()


router.get('/addFields',adminAuth,adminController.addFields);
router.get('/append',adminAuth,adminController.append);
router.get('/count',adminAuth,adminController.count);
router.get('/facet',adminAuth,adminController.facet);
router.get('/lookup',adminAuth,adminController.lookup);
router.get('/replaceroot',adminAuth,adminController.replaceRoot);

module.exports = router;
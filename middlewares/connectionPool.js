const mongoose = require('mongoose');

const uri = "mongodb+srv://vaghesh:admin@demo.jk4nr.mongodb.net/bankApp?retryWrites=true&w=majority"
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false,
    autoIndex: false, // Don't build indexes
    poolSize: 1, // Maintain up to 10 socket connections,
    serverSelectionTimeoutMS: 15000, // Keep trying to send operations for 5 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 ,// Use IPv4, skip trying IPv6
    bufferCommands:false
  };

mongoose.connect(uri,options)
        .then(()=>console.log(`Connection is ready with bankApp`))
        .catch((err)=>console.log(`Error in connection with DB`,err));

